/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phonebook.backend;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author george
 */
public class SearchCriteriaListModel extends AbstractListModel implements ComboBoxModel {

 
    
    public String[] criterias;
    public String selected;
    
    public SearchCriteriaListModel(){
        this.criterias = new String[SearchCriteriasEnum.values().length];
        
        for(SearchCriteriasEnum value : SearchCriteriasEnum.values()){
            this.criterias[value.ordinal()] = value.getLabel();
        }
        
    }
    
    @Override
    public int getSize() {
        return this.criterias.length;
    }

    @Override
    public Object getElementAt(int index) {
        return this.criterias[index];
    }

    @Override
    public void setSelectedItem(Object anItem) {
        this.selected = (String) anItem;
    }

    @Override
    public Object getSelectedItem() {
        return this.selected;
    }
    
}
