/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phonebook.backend;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author george
 */
public class Subscriber implements Comparable<Subscriber>, Serializable {

    private String firstName;
    private String lastName;
    private String cnp;
    private PhoneContact phoneContact;

    public Subscriber(String cnp, String firstName, String lastName, PhoneContact phoneContact) throws Exception {

        boolean validation;

        validation = FieldValidation.notEmpty(firstName) ;
        validation = validation && FieldValidation.notEmpty(cnp);
        validation = validation && FieldValidation.notEmpty(lastName);
        validation = validation && FieldValidation.notEmpty(phoneContact);

        if (validation == false){
            throw new Exception("Please fill in all the fields");
        }
        
        validation = FieldValidation.hasLength(cnp, 13);
        validation = validation && FieldValidation.isNumeric(cnp);
        
        if(validation == false){
            throw new Exception("Invalid CNP");
        }
        
        validation = FieldValidation.hasLength(phoneContact.getPhoneNumber().replace("-", ""), 10);
        validation = validation && FieldValidation.isNumeric(phoneContact.getPhoneNumber().replace("-", ""));
        
        if(validation == false){
            throw new Exception("Invalid Phone Number");
        }
        
        this.firstName = firstName;
        this.lastName = lastName;
        this.cnp = cnp;
        this.phoneContact = phoneContact;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public PhoneContact getPhoneContact() {
        return phoneContact;
    }

    public void setPhoneContact(PhoneContact phoneContact) {
        this.phoneContact = phoneContact;
    }

    @Override
    public String toString() {
        return "Subscriber{" + "firstName=" + firstName + ", lastName=" + lastName + ", cnp=" + cnp + ", phoneContact=" + phoneContact + '}';
    }

    @Override
    public int compareTo(Subscriber o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static Comparator<Subscriber> FirstNameComparator = new Comparator<Subscriber>() {

        @Override
        public int compare(Subscriber s1, Subscriber s2) {
            return s1.getFirstName().compareToIgnoreCase(s2.getFirstName());
        }
    };

    public static Comparator<Subscriber> LastNameComparator = new Comparator<Subscriber>() {

        @Override
        public int compare(Subscriber s1, Subscriber s2) {
            return s1.getLastName().compareToIgnoreCase(s2.getLastName());
        }
    };

    public static Comparator<Subscriber> CNPComparator = new Comparator<Subscriber>() {

        @Override
        public int compare(Subscriber s1, Subscriber s2) {
            return s1.getCnp().compareTo(s2.getCnp());
        }
    };

    public static Comparator<Subscriber> PhoneNumberComparator = new Comparator<Subscriber>() {

        @Override
        public int compare(Subscriber s1, Subscriber s2) {
            return s1.getPhoneContact().compareTo(s2.getPhoneContact());
        }
    };

}
