/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phonebook.backend;

/**
 *
 * @author george
 */
public enum SearchCriteriasEnum {

    ALL ("Show All"),
    FIRST_NAME("First Name"),
    LAST_NAME("Last Name"),
    CNP("CNP"),
    PHONE_NUMBER("Phone Number"),
    CONTACT_TYPE("Contact Type");

    private String label;

    SearchCriteriasEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

}
