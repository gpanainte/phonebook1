/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phonebook.backend;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import phonebook.backend.PhoneContact.ContactType;

/**
 *
 * @author george
 */
public class ContactTypeListModel extends AbstractListModel implements ComboBoxModel {

    String[] contactType;
    String selected;

    public ContactTypeListModel() {

        ContactType[] contactTypes = ContactType.values();
        this.contactType = new String[contactTypes.length];

        for (int i = 0; i < contactTypes.length; i++) {
            this.contactType[i] = contactTypes[i].name();
        }

    }

    @Override
    public void setSelectedItem(Object anItem) {
        this.selected = (String) anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selected;
    }

    @Override
    public int getSize() {
        return this.contactType.length;
    }

    @Override
    public Object getElementAt(int index) {
        return this.contactType[index];
    }

//    @Override
//    public void addListDataListener(ListDataListener l) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void removeListDataListener(ListDataListener l) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
}
