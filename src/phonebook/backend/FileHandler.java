/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phonebook.backend;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Map;

/**
 *
 * @author george
 */
public class FileHandler {

    public void save(File destination, Map<String, ? extends Serializable> data) throws FileNotFoundException, IOException {
        FileOutputStream fos;
        ObjectOutputStream objOut;

        fos = new FileOutputStream(destination);
        objOut = new ObjectOutputStream(fos);
        objOut.writeObject(data);

        objOut.close();
        fos.flush();
        fos.close();

    }

    public Map<String, ? extends Subscriber> open(File source) throws FileNotFoundException, IOException, ClassNotFoundException {

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(source));

        Map<String, ? extends Subscriber> data = (Map<String, ? extends Subscriber>) ois.readObject();

        ois.close();

        return data;

    }
    

}
