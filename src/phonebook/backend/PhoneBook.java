/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phonebook.backend;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author george
 */
public class PhoneBook implements Serializable {

    private Map<String, Subscriber> subscribers;
    private static PhoneBook instance;
    private Map<String, Subscriber> filterdSubscribers;
    private File source;

    private ArrayList<PhoneBookListener> listeners;

    private PhoneBook() {
        this.listeners = new ArrayList<>();
        this.subscribers = new HashMap<>();
        this.filterdSubscribers = new HashMap<>();

    }

    public void addListener(PhoneBookListener listener) {
        this.listeners.add(listener);
    }

    public static PhoneBook getInstance() {
        if (instance == null) {
            instance = new PhoneBook();
        }
        return instance;
    }

    public File getSource() {
        return source;
    }

    public void setSource(File source) {
        this.source = source;
        this.notifyListeners();
    }

    public Map<String, Subscriber> getSubscribers() {
        if (this.filterdSubscribers.isEmpty()) {
            return this.subscribers;
        } else {
            return this.filterdSubscribers;
        }
    }

    public Map<String, Subscriber> getSubscribers(boolean allSubscribers) {
        if (allSubscribers == true) {
            return this.subscribers;
        } else {
            return this.getSubscribers();
        }
    }

    public void updatePhoneBook(Map<String, Subscriber> subscribers) throws Exception {
        this.subscribers.clear();

        for (Subscriber s : subscribers.values()) {
            this.saveSubscriber(s, OperationsEnum.INSERT);
        }

    }

    private void notifyListeners() {
        for (PhoneBookListener listener : this.listeners) {
            listener.phoneBookUpdated();
        }
    }

    public void saveSubscriber(Subscriber subscriber, OperationsEnum operation) throws Exception {

        if (operation == OperationsEnum.INSERT && this.subscribers.containsKey(subscriber.getCnp())) {
            throw new Exception("Subscriber already exists");
        }

        this.subscribers.put(subscriber.getCnp(), subscriber);
        this.notifyListeners();

    }

    public void deleteSubscriber(Subscriber subscriber) {
        this.subscribers.remove(subscriber.getCnp());
        this.notifyListeners();
    }

    public void resetFilter() {
        this.filterdSubscribers.clear();
        this.notifyListeners();
    }

    public void filter(SearchCriteriasEnum search, String data) {

        SearchCriteriasEnum s = search;

        switch (s) {
            case FIRST_NAME:
                this.searchByFirstName(data);
                break;
            case ALL:
                this.resetFilter();
                break;
            case LAST_NAME:
                this.searchByLastName(data);
                break;
            case CNP:
                this.searchByCNP(data);
                break;
            case PHONE_NUMBER:
                this.searchByContactNumber(data);
                break;
            case CONTACT_TYPE:
                this.searchByContactType(data);
                break;
            default:
                this.resetFilter();

        }
    }

    private void searchByFirstName(String name) {

        this.resetFilter();

        for (Map.Entry<String, Subscriber> subscriber : this.subscribers.entrySet()) {
            if (subscriber.getValue().getFirstName().toLowerCase().contains(name.toLowerCase())) {
                this.filterdSubscribers.put(subscriber.getKey(), subscriber.getValue());
            }
        }

        this.notifyListeners();

    }

    private void searchByLastName(String name) {
        this.resetFilter();

        for (Map.Entry<String, Subscriber> subscriber : this.subscribers.entrySet()) {
            if (subscriber.getValue().getLastName().toLowerCase().contains(name.toLowerCase())) {
                this.filterdSubscribers.put(subscriber.getKey(), subscriber.getValue());
            }
        }

        this.notifyListeners();
    }

    private void searchByCNP(String cnp) {
        this.resetFilter();

        for (Map.Entry<String, Subscriber> subscriber : this.subscribers.entrySet()) {
            if (subscriber.getValue().getCnp().contains(cnp)) {
                this.filterdSubscribers.put(subscriber.getKey(), subscriber.getValue());
            }
        }

        this.notifyListeners();
    }

    private void searchByContactNumber(String number) {

        this.resetFilter();

        for (Map.Entry<String, Subscriber> subscriber : this.subscribers.entrySet()) {
            if (subscriber.getValue().getPhoneContact().getPhoneNumber().contains(number)) {
                this.filterdSubscribers.put(subscriber.getKey(), subscriber.getValue());
            }
        }

        this.notifyListeners();

    }

    private void searchByContactType(String type) {
        this.resetFilter();

        for (Map.Entry<String, Subscriber> subscriber : this.subscribers.entrySet()) {
            if (subscriber.getValue().getPhoneContact().getContactType().name().toLowerCase().equals(type.toLowerCase())) {
                this.filterdSubscribers.put(subscriber.getKey(), subscriber.getValue());
            }
        }

        this.notifyListeners();
    }

    public void saveToFile() throws IOException {
        FileHandler fh = new FileHandler();
        fh.save(this.source, this.getSubscribers(true));
    }

    public void loadFromFile() throws IOException, Exception {
        FileHandler fh = new FileHandler();
        this.updatePhoneBook((Map<String, Subscriber>) fh.open(this.source));
    }  

}
