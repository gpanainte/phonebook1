/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phonebook.backend;

import java.io.Serializable;
import java.util.Map;

/**
 *
 * @author george
 */
public class SubscriberTableModel extends ExtendedTableModel implements Serializable, PhoneBookListener/*, TableModelListener*/ {

    public enum Columns {

        CNP("CNP", 0),
        FIRST_NAME("First Name", 1),
        LAST_NAME("Last Name", 2),
        CONTACT_TYPE("Contact Type", 3),
        PHONE_NUMBER("Phone Number", 4);

        private String columnName;
        private int index;

        private Columns(String name, int index) {
            this.columnName = name;
            this.index = index;

        }

        public String getName() {
            return this.columnName;
        }

        public int getIndex() {
            return this.index;
        }
    };
    private String[] columnNames = {Columns.CNP.getName(), Columns.FIRST_NAME.getName(), Columns.LAST_NAME.getName(), Columns.CONTACT_TYPE.getName(), Columns.PHONE_NUMBER.getName()};
    private Object[][] data;
    private final PhoneBook pb;

    public SubscriberTableModel() {
        this.pb = PhoneBook.getInstance();
        pb.addListener(this);
    }

    public void setData(PhoneBook phoneBook) {
        this.data = new Object[phoneBook.getSubscribers().size()][];
        int i = 0;
        for (Map.Entry<String, Subscriber> subscriber : phoneBook.getSubscribers().entrySet()) {

            this.data[i] = new Object[Columns.values().length];
            this.data[i][Columns.CNP.getIndex()] = subscriber.getValue().getCnp();
            this.data[i][Columns.FIRST_NAME.getIndex()] = subscriber.getValue().getFirstName();
            this.data[i][Columns.LAST_NAME.index] = subscriber.getValue().getLastName();
            this.data[i][Columns.CONTACT_TYPE.getIndex()] = subscriber.getValue().getPhoneContact().getContactType();
            this.data[i][Columns.PHONE_NUMBER.getIndex()] = subscriber.getValue().getPhoneContact().getPhoneNumber();

            i++;
        }
    }

    @Override
    public int getRowCount() {
        return this.data.length;
    }

    @Override
    public int getColumnCount() {
        return this.columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return data[rowIndex][columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
//        return super.isCellEditable(rowIndex, columnIndex); //To change body of generated methods, choose Tools | Templates.
        return true;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return this.getValueAt(0, columnIndex).getClass();
    }

    @Override
    public String getColumnName(int column) {
        return this.columnNames[column];
    }

    @Override
    public void phoneBookUpdated() {
        this.setData(this.pb);
        this.fireTableDataChanged();
    }

    @Override
    public Subscriber getRowDataAt(int index) {
        String cnp = (String) this.getValueAt(index, Columns.CNP.getIndex());

        return this.pb.getSubscribers().get(cnp);

    }

//    @Override
//    public void tableChanged(TableModelEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
}
