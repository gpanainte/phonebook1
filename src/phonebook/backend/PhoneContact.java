/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phonebook.backend;

import java.io.Serializable;

/**
 *
 * @author george
 */
public class PhoneContact implements Comparable<PhoneContact>, Serializable{

    public enum ContactType{FIXED, MOBILE}
    
    private ContactType contactType;
    private String phoneNumber;

    public PhoneContact(ContactType contactType, String phoneNumber) throws Exception {
        
        boolean validate;
        
        validate = FieldValidation.notEmpty(contactType);
        validate = validate && FieldValidation.notEmpty(phoneNumber);
        
        if(validate == false){
            throw new Exception("Fill in all the required fields");
        }
        
        this.contactType = contactType;
        this.phoneNumber = phoneNumber;
    }

    public ContactType getContactType() {
        return contactType;
    }

    public void setContactType(ContactType contactType) {
        this.contactType = contactType;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "PhoneContact{" + "contactType=" + contactType + ", phoneNumber=" + phoneNumber + '}';
    }
    
  @Override
    public int compareTo(PhoneContact p) {
        return this.getPhoneNumber().compareTo(p.getPhoneNumber());
    }
    
    
    
}
