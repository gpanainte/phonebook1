/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phonebook.backend;

/**
 *
 * @author george
 */
public enum OperationsEnum {

    INSERT("I"),
    UPDATE("D");

    String operation;

    OperationsEnum(String operation) {
        this.operation = operation;
    }
    
    public String getOperation(){
        return this.operation;
    }
}
