/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phonebook.backend;

/**
 *
 * @author george
 */
public class FieldValidation {

    public static boolean notEmpty(String value) {
        return !value.isEmpty();
    }
    
    public static boolean notEmpty(Object value){
        return value != null;
    }

    public static boolean isNumeric(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean hasLength(String value, int length) {
        return value.length() == length;
    }

}
