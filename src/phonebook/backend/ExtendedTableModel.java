/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phonebook.backend;

import javax.swing.table.AbstractTableModel;

/**
 *
 * @author george
 */
public abstract class ExtendedTableModel extends AbstractTableModel {
    
    public abstract Object getRowDataAt(int index);
    
}
